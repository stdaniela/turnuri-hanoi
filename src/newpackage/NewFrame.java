package newpackage;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class NewFrame extends JFrame {
    Hanoi h;
    JButton startButton;
    JButton resetButton;
    
    JFrame frame = this;
    
    public NewFrame() {
        super("Turnurile din Hanoi");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400,400);
        setResizable(false);
        setVisible(true);//pentru a avea fereastra vizibila inainte de afisarea mesajelor
        
        startButton = new JButton("START");
        resetButton = new JButton("RESET");
        
        startButton.setEnabled(true);
        resetButton.setEnabled(false);
        
        BorderLayout layout = new BorderLayout();
        setLayout(layout);
        JPanel panel = new JPanel();
        GridLayout gridLayout = new GridLayout(1,2,5,5);
        panel.setLayout(gridLayout);
        panel.add(startButton);
        panel.add(resetButton);
        add(panel,BorderLayout.NORTH);
        
        
        boolean ok = true;
        while (ok) {
            String op = JOptionPane.showInputDialog(this, "Introduceti numarul de discuri (n <= 10):");
            System.out.println("op = " + op);
            try {
                if (Integer.parseInt(op) <= 10) {
                    h = new Hanoi(Integer.parseInt(op));
                    add(h,BorderLayout.CENTER);
                    ok = false;
                } else {
                    JOptionPane.showMessageDialog(this, "Introduceti o valoare mai mica sau egala cu 10", "Atentie", JOptionPane.WARNING_MESSAGE);
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(this, "Introduceti numai valori intregi", "Eroare", JOptionPane.ERROR_MESSAGE);
            }
        }
        
        startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                startButton.setEnabled(false);
                resetButton.setEnabled(false);
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        h.mutareDiscuri();
                        resetButton.setEnabled(true);
                    }
                });
                thread.start();
                }
        });
        resetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                startButton.setEnabled(true);
                boolean ok = true;
                while (ok) {
                    String op = JOptionPane.showInputDialog(frame, "Introduceti numarul de discuri (n <= 10):");
                    try {
                        if (Integer.parseInt(op) <= 10) {
                            h.reset(Integer.parseInt(op));
                            add(h,BorderLayout.CENTER);
                            ok = false;
                        } else {
                            JOptionPane.showMessageDialog(frame, "Introduceti o valoare mai mica sau egala cu 10", "Atentie", JOptionPane.WARNING_MESSAGE);
                        }
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(frame, "Introduceti numai valori intregi", "Eroare", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        });
        
        setVisible(true);//pentru a face vizibil si panel-ul pe care l-am adaugat
        //ulterior apelarii lui setVisible() de la linia 11
    }
}
