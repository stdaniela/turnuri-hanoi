package newpackage;

import java.awt.*;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import javax.swing.*;

public final class Hanoi extends JPanel {
    // numarul de discuri
    private int discNumber;
    
    // pozitiile celor 3 turnuri
    Point t1P = new Point(50,300);
    Point t2P= new Point(160,300);
    Point t3P = new Point(270,300);
    
    //tablourile ce contin cele n discuri
    ArrayList<Rectangle1> turn1; //initial contine toate discurile
    ArrayList<Rectangle1> turn2; //in final va avea toate discurile
    ArrayList<Rectangle1> turn3; //este folosit ca turn intermediar; la inceput si la sfarsit acesta va fi gol
    
    //lista cu mutarile care trebuie facute
    ArrayList<Turnuri> listaMutari;
    
    public Hanoi(int number) {
        //numarul de discuri
        discNumber = number;
        
        reset(discNumber);
    }
    
    public void reset(int number) {
        listaMutari = new ArrayList<>();
        
        discNumber = number;
        
        //initializarea listei de mutari (pentru un numar mare de discuri va fi o problema)
        hanoi(discNumber,1,2);
        
        turn1 = new ArrayList<>();
        turn2 = new ArrayList<>();
        turn3 = new ArrayList<>();
        
        //initializarea turnului 1; primul disc din lista va fi cel mai mare, ultimul cel mai mic
        for (int count = discNumber; count >= 1; count--) {
            turn1.add(new Rectangle1(
                    t1P.getX() - 5 * count,
                    t1P.getY() - 20 * (discNumber + 1 - count),
                    10 * count,
                    20,
                    new Color(
                            (int)(Math.random() * 255),
                            (int)(Math.random() * 255),
                            (int)(Math.random() * 255))
                )
            );
        }
        repaint();
    }
    
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(Color.BLACK);
        
        //primul turn
        g.drawLine(10, 300, 90, 300);
        g.drawLine(50, 300, 50, 200);
        g.drawString("1", 50, 320);
        
        //al doilea turn
        g.drawLine(120, 300, 200, 300);
        g.drawLine(160, 300, 160, 200);
        g.drawString("2", 160, 320);
        
        //al treilea turn
        g.drawLine(230, 300, 310, 300);
        g.drawLine(270, 300, 270, 200);
        g.drawString("3", 270, 320);
        
        if (turn1 != null) {
            turn1.stream().forEach((t1) -> {
                g.setColor(t1.getColor());
                g.fillRect(t1.getX(), t1.getY(), t1.getW(), t1.getH());
            });
        }
        
        if (turn2 != null) {
            turn2.stream().forEach((t2) -> {
                g.setColor(t2.getColor());
                g.fillRect(t2.getX(), t2.getY(), t2.getW(), t2.getH());
            });
        }
        
        if (turn3 != null) {
            turn3.stream().forEach((t3) -> {
                g.setColor(t3.getColor());
                g.fillRect(t3.getX(), t3.getY(), t3.getW(), t3.getH());
            });
        }
    }
    
    public final void mutareDiscuri() {
        ArrayList<Rectangle1> turn_i;
        ArrayList<Rectangle1> turn_j;
        Point t_j;
        for (Turnuri t : listaMutari) {
            if  ( (t.i == 1) && (t.j == 2) ) {
                turn_i = turn1;
                turn_j = turn2;
                t_j = t2P;
            } else if  ( (t.i == 1) && (t.j == 3) ) {
                turn_i = turn1;
                turn_j = turn3;
                t_j = t3P;
            } else if  ( (t.i == 2) && (t.j == 1) ) {
                turn_i = turn2;
                turn_j = turn1;
                t_j = t1P;
            } else if  ( (t.i == 2) && (t.j == 3) ) {
                turn_i = turn2;
                turn_j = turn3;
                t_j = t3P;
            } else if  ( (t.i == 3) && (t.j == 1) ) {
                turn_i = turn3;
                turn_j = turn1;
                t_j = t1P;
            } else {
                turn_i = turn3;
                turn_j = turn2;
                t_j = t2P;
            }
            
            int size;
            if (turn_j != null) {
                size = turn_j.size();
            } else {
                size = 0;
            }
            turn_j.add(new Rectangle1(
                    t_j.getX() - (int)(turn_i.get(turn_i.size() - 1).getW()/2),
                    t_j.getY() - 20 * (size + 1),
                    turn_i.get(turn_i.size() - 1).getW(),
                    turn_i.get(turn_i.size() - 1).getH(),
                    turn_i.get(turn_i.size() - 1).getColor()
                )
            );
            
            
            turn_i.remove(turn_i.size() - 1);
            
            repaint();
            timeDelay(1);
        }
    }
    
    private void timeDelay(int x) {
        try {
            TimeUnit.SECONDS.sleep(x);
        } catch (InterruptedException e) {
            System.out.println(e);
        }
    }
    
    public final void hanoi(int n, int i, int j) {
        if (n > 0) {
            hanoi(n-1, i, 6-i-j);
            listaMutari.add(new Turnuri(i,j));
            hanoi(n-1,6-i-j,j);
        }
    }
    
    class Turnuri {
        private int i, j;
        
        public Turnuri(int i, int j) {
            if ( (i >= 1) && (i <= 3) ) {
                this.i = i;
            }
            
            if ( (i >= 1) && (i <= 3) ) {
                this.j = j;
            }
        }
    }
}
