package newpackage;

public class Point {
    private int x,y;
    
    public Point(int x, int y) {
        if ( (x >= 0) && (y >= 0) ) {
            this.x = x;
            this.y = y;
        }
    }
    
    public void setX(int x) {
        if (x >= 0) {
            this.x = x;
        } else {
            System.out.println("Introduceti o valoare pozitiva (in pixeli)");
        }
    }
    
    public int getX() {
        return x;
    }
    
    public void setY(int y) {
        if (x >= 0) {
            this.y = y;
        } else {
            System.out.println("Introduceti o valoare pozitiva (in pixeli)");
        }
    }
    
    public int getY() {
        return y;
    }
    
    public void printCoord() {
        System.out.print(x + "," + y);
    }
}
