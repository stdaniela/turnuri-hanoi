package newpackage;

import java.awt.*;

public class Rectangle1 {
    //coordonatele punctului stanga sus al dreptunghiului
    private int x, y;
    //lungimea si latimea (inaltimea) dreptunghiului
    private int w, h;
    //culoarea dreptunghiului
    private Color color;
    
    public Rectangle1(int x, int y, int w, int h, Color color) {
        if (color != Color.LIGHT_GRAY) {
            this.color = color;
        }
        
        if (x > 0) {
            this.x = x;
        } else {
            System.out.println("Introduceti o valoare pozitiva");
        }
        
        if (y > 0) {
            this.y = y;
        } else {
            System.out.println("Introduceti o valoare pozitiva");
        }
        
        if (w > 0) {
            this.w = w;
        } else {
            System.out.println("Introduceti o valoare pozitiva");
        }
        
        if (h > 0) {
            this.h = h;
        } else {
            System.out.println("Introduceti o valoare pozitiva");
        }
    }
    
    public void setX(int x) {
        if (x > 0) {
            this.x = x;
        } else {
            System.out.println("Introduceti o valoare pozitiva");
        }
    }
    
    public void setY1(int y) {
        if (y > 0) {
            this.y = y;
        } else {
            System.out.println("Introduceti o valoare pozitiva");
        }
    }
    
    public void setW(int w) {
        if (w > 0) {
            this.w = w;
        } else {
            System.out.println("Introduceti o valoare pozitiva");
        }
    }
    
    public void setH(int h) {
        if (h > 0) {
            this.h = h;
        } else {
            System.out.println("Introduceti o valoare pozitiva");
        }
    }
    
    public int getX() {
        return x;
    }
    
    public int getY() {
        return y;
    }
    
    public int getW() {
        return w;
    }
    
    public int getH() {
        return h;
    }
    
    public Color getColor() {
        return color;
    }
    
    public void setColor(Color color) {
        if (color != Color.LIGHT_GRAY) {
            this.color = color;
        }
    }
}
